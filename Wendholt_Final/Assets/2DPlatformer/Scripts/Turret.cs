﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : Drone
{
    public GameObject lazer;
    private Camera c;
    private float xPosition;
    private float yPosition;
    private float xDistanceToAnchor;
    private float yDistanceToAnchor;
    void Start()
    {
        c = Camera.main;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            FireLazer(Input.mousePosition);
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        Vector3 worldPos = c.ScreenToWorldPoint(Input.mousePosition);
        Vector2 worldPoint2d = new Vector2(worldPos.x, worldPos.y);
        // keeping the x position of the turret close to the player

        xPosition = Mathf.Clamp(worldPoint2d.x, anchor.transform.position.x - .5f, anchor.transform.position.x + .5f);
        yPosition = Mathf.Clamp(worldPoint2d.y, anchor.transform.position.y - .5f, anchor.transform.position.y + .5f);
        transform.position = new Vector2(xPosition, yPosition);
    }

    void FireLazer(Vector2 mousePosition)
    {
        Vector3 worldPos = c.ScreenToWorldPoint(Input.mousePosition);
        Vector2 worldPoint2d = new Vector2(worldPos.x, worldPos.y);
        Vector2 forward = worldPoint2d - new Vector2 (transform.position.x, transform.position.y);
        Instantiate(lazer, new Vector2(transform.position.x,transform.position.y), transform.rotation);
    }
}

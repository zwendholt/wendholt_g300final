﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Claw : MonoBehaviour
{
    public GameObject anchor;
    public static GameObject claw;

    public PlayerPlatformerController player;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Awake()
    {
        if(claw == null)
        {
            claw = gameObject;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        Follow(anchor);
    }

    public void Follow(GameObject anchor)
    {
        Vector2 dirToPlayer = anchor.transform.position - transform.position;
        //finding the direction to the anchor of the player
        dirToPlayer = dirToPlayer.normalized;
        // finding how far the shield is to the anchor
        float distanceToAnchor = Vector2.Distance(anchor.transform.position, transform.position);
        //
        transform.position = new Vector2(anchor.transform.position.x, anchor.transform.position.y);
        //rb2d.AddForce(dirToPlayer * (speed*distanceToAnchor));
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Platform"))
        {
            player.setClimbVariable(true);
        }
        else
        {
            player.setClimbVariable(false);
        }
    }


}

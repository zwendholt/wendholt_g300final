﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drone : MonoBehaviour
{
    public float speed = 10.0f;
    public GameObject anchor;
    Rigidbody2D rb2d;
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Follow(anchor);
    }

    public void Follow(GameObject anchor)
    {
        Vector2 dirToPlayer = anchor.transform.position - transform.position;
        //finding the direction to the anchor of the player
        dirToPlayer = dirToPlayer.normalized;
        // finding how far the shield is to the anchor
        float distanceToAnchor = Vector2.Distance(anchor.transform.position, transform.position);
        //
        transform.position = new Vector2(anchor.transform.position.x, anchor.transform.position.y);
        //rb2d.AddForce(dirToPlayer * (speed*distanceToAnchor));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Knife : MonoBehaviour
{
    public static GameObject knife;
    public GameObject leftAnchor;
    public GameObject rightAnchor;
    public bool movingRight;
    // Start is called before the first frame update
    void Start()
    {
        movingRight = true;
    }

    void Awake() 
    {
        if (knife == null)
        {
            knife = gameObject;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(movingRight)
        {
            Follow(rightAnchor);
        }
        else
        {
            Follow(leftAnchor);
        }
    }

    public void Follow(GameObject anchor)
    {
        Vector2 dirToPlayer = anchor.transform.position - transform.position;
        //finding the direction to the anchor of the player
        dirToPlayer = dirToPlayer.normalized;
        // finding how far the shield is to the anchor
        float distanceToAnchor = Vector2.Distance(anchor.transform.position, transform.position);
        //
        transform.position = new Vector2(anchor.transform.position.x, anchor.transform.position.y);
        //rb2d.AddForce(dirToPlayer * (speed*distanceToAnchor));
    }

    public void LookingRight()
    {
        movingRight = true;
    }

    public void LookingLeft()
    {
        movingRight = false;
    }

    void OnTriggerEnter2D(Collider2D other)
	{
        if (other.gameObject.CompareTag("BreakableWall"))
        {
            Destroy(other.gameObject);
        }
    }

}

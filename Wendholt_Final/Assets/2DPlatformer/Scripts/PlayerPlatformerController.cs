﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerPlatformerController : PhysicsObject {

    public float maxSpeed = 7;
    private float jumpTakeOffSpeed = 5;
    private int ram;
    private bool climbBool;
    private bool swimming;
    private bool hasClaw;
    private bool hasGills;

    private SpriteRenderer spriteRenderer;
    private Animator animator;

    public Knife knife;
    public static PlayerPlatformerController player; 
    

    // Use this for initialization
    void Awake () 
    {
        // the player starts the game with no RAM (therefore can't control drones)
        ram = 0;
        spriteRenderer = GetComponent<SpriteRenderer> ();    
        animator = GetComponent<Animator> ();
        knife.GetComponent<SpriteRenderer>().enabled = false;
        knife.GetComponent<BoxCollider2D>().enabled = false;
        hasClaw = false;
        hasGills = false;

        if (player == null)
        {
            print("im the player now");
            player = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            print("get out of here");
            Destroy(gameObject);
        }
    }

    void Update(){
        targetVelocity = Vector2.zero;
        ComputeVelocity();

        if (ram >= 3)
        {
            SceneManager.LoadScene("EndScene", LoadSceneMode.Single);
        }

        if(Input.GetKey(KeyCode.Return) && hasClaw)
        {
            knife.GetComponent<SpriteRenderer>().enabled = true;
            knife.GetComponent<BoxCollider2D>().enabled = true;
        }
        else
        {
            knife.GetComponent<SpriteRenderer>().enabled = false;
            knife.GetComponent<BoxCollider2D>().enabled = false;
        }
    }
    protected override void ComputeVelocity()
    {
        if(transform.position.y < -100)
        {
            RestartLevel();
        }
        Vector2 move = Vector2.zero;

        move.x = Input.GetAxis ("Horizontal");

        if(move.x > .01)
        {
            knife.LookingRight();
        }
        if(move.x < -.01)
        {
            knife.LookingLeft();
        }
        // vanilla jump
        if (Input.GetButtonDown ("Jump") && grounded) {
            velocity.y = jumpTakeOffSpeed;
        } else if (Input.GetButtonUp ("Jump")) 
        {
            if (velocity.y > 0) {
                velocity.y = velocity.y * 0.2f;
            }
        }

        // the input test for climbing
        if(Input.GetButtonDown("Jump") && climbBool && hasClaw && !grounded && !swimming)
        {
            velocity.y = jumpTakeOffSpeed;
        }

        //swimming jump
        if(Input.GetButtonDown("Jump") && swimming)
        {
            velocity.y = jumpTakeOffSpeed;
        }

        bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < -0.01f));
        if (flipSprite) 
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }

        animator.SetBool ("grounded", grounded);
        animator.SetFloat ("velocityX", Mathf.Abs (velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;
    }


    void OnTriggerEnter2D(Collider2D other)
	{

         if (other.gameObject.CompareTag("Water"))
        {
            if(hasGills)
            {
                swimming = true;
                gravityModifier = .4f;
            }
            else
            {
                RestartLevel();
            }

        }

        if (other.gameObject.CompareTag("LevelOnePortal"))
        {
            SceneManager.LoadScene("LevelOne", LoadSceneMode.Single);
        }

        if (other.gameObject.CompareTag("LevelTwoPortal"))
        {
            SceneManager.LoadScene("LevelTwo", LoadSceneMode.Single);
        }

        if (other.gameObject.CompareTag("Window"))
        {
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            SceneManager.LoadScene("EndScene", LoadSceneMode.Single);
        }

        // if the player collides with a ram game object
        if (other.gameObject.CompareTag("Claw"))
        {
            other.gameObject.SetActive(false);
            hasClaw = true;
            ram = ram + 1;
        }

        if (other.gameObject.CompareTag("Gills"))
        {
            other.gameObject.SetActive(false);
            hasGills = true;
            ram = ram + 1;
        }
        
        // if the player collides with an enemy
        if (other.gameObject.CompareTag("Enemy"))
        {
            RestartLevel();
        }
	}

    void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.CompareTag("Water"))
        {
            swimming = false;
            gravityModifier = 1;
        }
    }
    
    public void RestartLevel()
    {
        // transform the player to the starting location
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        transform.position = new Vector2(-3.0f, -2.54f);
    }

    public void setClimbVariable(bool x)
    {
        climbBool = x;
    }
}